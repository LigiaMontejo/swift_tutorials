//
//  TaskManager.swift
//  My Task List
//
//  Created by Ligia Montejo on 6/13/15.
//  Copyright (c) 2015 MobileAndCloudCo. All rights reserved.
//

import UIKit

var taskMgr: TaskManager = TaskManager()
struct task{
    var name = "un-Named"
    var desc = "un-Described"
}

class TaskManager: NSObject {
   
    var tasks = [task]()
    
    func addTask(name:String, desc:String){
        tasks.append(task(name: name, desc:desc))
    }
    
}
