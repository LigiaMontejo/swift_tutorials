class Animal{
    var name = "default"
    var age = 0;
    
    func getDetail() -> String{
        return "this animals name is \(name) it is \(age) years old."
    }
}

class Dog : Animal{
    func woof() -> String{
        return "woof woof woof"
    }
}

class Cat : Animal{
    func meow() -> String{
        return "meow meow meow"
    }
}


var LigiaDog = Dog()
LigiaDog.name = "Lucho"
LigiaDog.age = 16
LigiaDog.woof()
LigiaDog.getDetail()

var LigiaCat = Cat()
LigiaCat.name = "Lucky"
LigiaCat.age = 1
LigiaCat.meow()
LigiaCat.getDetail()

var JonsCat = Cat()
var DaveCat = Cat()


