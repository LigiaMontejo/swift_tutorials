// Playground - noun: a place where people can play

var fruit = "apples"
var myfruit = "bananas"

switch fruit{
    case "apples":
        println("The fruit was some \(fruit)")
    case "bananas":
        println("The fruit was a bunch of \(myfruit)")
    default:
        println("No fruit matched")
    
}
