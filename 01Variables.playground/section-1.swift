// Playground - noun: a place where people can play

import UIKit

var name = "Ligia"
var title = "Ms"

var returnValue = "Hello \(title) \(name)"

var strongName:String = "Ryan"

var score = 125
var multi = 4

var output = "Your score is \(score*multi)"
